import { Component, OnInit, Input } from '@angular/core';
import { User } from './../model/user.model';

@Component({
  selector: 'app-plus',
  templateUrl: './plus.component.html',
  styleUrls: ['./plus.component.css']
})
export class PlusComponent implements OnInit {

  inputTask:string="";
  inputDesciption:string="";
  inputDate:string="";
  inputTime:string="";
  @Input('users') users:User[];

  constructor() { }

  ngOnInit(): void {
  }
  addTodo(){
    this.users.push({
      tasks:this.inputTask,
      description:this.inputDesciption,
      date:this.inputDate,
      time:this.inputTime,
      important:false
    });
    console.log(this.users);
    this.inputTask="";
    this.inputDesciption="";
    this.inputDate="";
    this.inputTime="";
    
  }

}
