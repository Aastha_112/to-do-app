export class User{
    tasks:string;
    description:string;
    date:string;
    time:string;
    important:boolean=false;
    constructor(){
        this.tasks='';
        this.description='';
        this.date='';
        this.time='';
    }
}