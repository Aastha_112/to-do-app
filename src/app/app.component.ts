import { Component } from '@angular/core';
import { User } from './model/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  menulists=['My-Day','Important','Planned','Tasks'];
  icons=['fa-sun-o','fa-star-o','fa-calendar','fa-home'];
  selectedList:any;
  important:boolean=false;
  opened:boolean=true;
  users: User[];
  constructor(){
    this.users=[];
  }

  ngOnInit(): void {
  }
  openMenuList(menulist:any){
    this.selectedList=menulist;
  }
  openSideBar(){
    this.opened=!this.opened;
  }
}
