import { Component, OnInit, Input } from '@angular/core';
import { User } from './../model/user.model';
import { DatePipe } from '@angular/common';
import {DpDatePickerModule} from 'ng2-date-picker';

@Component({
  selector: 'app-my-day',
  templateUrl: './my-day.component.html',
  styleUrls: ['./my-day.component.css']
})
export class MyDayComponent implements OnInit {

  today: number = Date.now();
  currentDate = this.datepipe.transform(this.today, 'yyyy-MM-dd');
  inputTask:string="";
  inputDesciption:string="";
  inputDate:string="";
  inputTime:string="";
  @Input('users') users:User[];
  constructor(public datepipe: DatePipe) {
   }

  ngOnInit() {
    
  }

  addTodo(){
    this.users.push({
      tasks:this.inputTask,
      description:this.inputDesciption,
      date:this.inputDate,
      time:this.inputTime,
      important:false
    });
    console.log(this.users);
    this.inputTask="";
    this.inputDesciption="";
    this.inputDate="";
    this.inputTime="";
    
  }

  addImportant(count:number){
    this.users.map((v,i)=>{
      if(i==count){
        v.important=!v.important
      }
      console.log(v);
        return v;
    })
  }

}
