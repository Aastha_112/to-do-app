import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { TasksComponent } from './tasks/tasks.component';
import { MyDayComponent } from './my-day/my-day.component';
import { ImportantComponent } from './important/important.component';
import { PlannedComponent } from './planned/planned.component';

const routes: Routes = [
  // { path: '', component: AppComponent },
  // { path: 'my-day', component: MyDayComponent },
  // { path: 'tasks-component', component: TasksComponent },
  // { path: 'important', component: ImportantComponent },
  // { path: 'planned', component: PlannedComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
