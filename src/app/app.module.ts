import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyDayComponent } from './my-day/my-day.component';
import { TasksComponent } from './tasks/tasks.component';
import { PlannedComponent } from './planned/planned.component';
import { ImportantComponent } from './important/important.component';
import { DatePipe } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import {DpDatePickerModule} from 'ng2-date-picker';
import { SidebarModule } from 'ng-sidebar';
import { PlusComponent } from './plus/plus.component';

@NgModule({
  declarations: [
    AppComponent,
    MyDayComponent,
    TasksComponent,
    PlannedComponent,
    ImportantComponent,
    HeaderComponent,
    PlusComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    DpDatePickerModule,
    SidebarModule.forRoot()
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
