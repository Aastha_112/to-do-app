import { Component, OnInit, Input } from '@angular/core';
import { User } from './../model/user.model';

@Component({
  selector: 'app-important',
  templateUrl: './important.component.html',
  styleUrls: ['./important.component.css']
})
export class ImportantComponent implements OnInit {

  @Input('users') users:User[];

  constructor() { }

  ngOnInit(): void {
  }

  addImportant(count:number){
    this.users.map((v,i)=>{
      if(i==count){
        v.important=!v.important
      }
      console.log(v);
      return v;
    })
  }

}
